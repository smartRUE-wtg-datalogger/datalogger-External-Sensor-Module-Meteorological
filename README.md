A set of meteorological sensor components (wind speed and direction, air 
pressure, temperature and relative humidity) provisioned for interconnection 
with the smartRUE datalogger core module, designed to comply with IEC 
specifications.

In this repository all the design issues are addressed, and reusable schematic 
and pcb design components are developed. 